# Prepare to Die Edition Version Data  

This folder contains version data for the [PtDE branch](https://github.com/metal-crow/Dark-Souls-1-Overhaul/tree/PtDE) of the Overhaul project for [Dark Souls™: Prepare to Die™ Edition](https://store.steampowered.com/app/211420).  
