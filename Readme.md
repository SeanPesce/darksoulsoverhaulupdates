# Dark Souls Overhaul Version Data  

This is a backup repository for live strings fetched by the [Dark Souls Overhaul](https://github.com/metal-crow/Dark-Souls-1-Overhaul) project.  

 * **Message of the Day (MotD)** - Message containing miscellanious information pertaining to Overhaul users.  
 * **Version** - Version string containing the compilation date/time of the [latest official build](https://github.com/metal-crow/Dark-Souls-1-Overhaul/releases) of the Overhaul DLL.  
 * **Download URL** - URL to download the latest official build.  

 ____________________________________
 
 
 Maintained by [Sean Pesce](https://SeanPesce.github.io).  
 